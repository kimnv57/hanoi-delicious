package com.example.adapter;

import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hanoi_delicious2.R;


// class apdater for list monan
public class LowItemAdapter extends ArrayAdapter<LowItem>{
	ArrayList<LowItem> itemList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;

	public LowItemAdapter(Context context, int resource, ArrayList<LowItem> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		itemList = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		View v = convertView;
//		if (v == null) {
//			holder = new ViewHolder();
//			v = vi.inflate(Resource, null);
//			holder.imageview = (ImageView) v.findViewById(R.id.ImageView1);
//			holder.nameview = (TextView) v.findViewById(R.id.name);
//			v.setTag(holder);
//			holder.imageview.setImageResource(R.drawable.loading);
//			new DownloadImageTask(holder.imageview).execute(itemList.get(position).getImage());
//			holder.nameview.setText(itemList.get(position).getName());
//			return v;
//		} else {
//			//holder = (ViewHolder) v.getTag();
//			return v;
//		}
		Log.d("adapter", ""+ position);
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.ImageView1);
			holder.nameview = (TextView) v.findViewById(R.id.name);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.imageview.setImageResource(R.drawable.loading);
		new DownloadImageTask(holder.imageview).execute(itemList.get(position).getImage());
		holder.nameview.setText(itemList.get(position).getName());
		return v;
		
		
	}
	// download image from network
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}

	}
	
	
	static class ViewHolder {
		public ImageView imageview;
		public TextView nameview;

	}

}
