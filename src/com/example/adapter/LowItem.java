package com.example.adapter;


// Monan low details ( information of each item on list Monan)
public class LowItem {
	
	private String Image;
	private String Name;
	
	public LowItem() {
		//do nothing
	}
	
	public LowItem(String image,String name,String place) {
		super();
		this.Image=image;
		this.Name=name;
	}
	
	
	//name
	public String getName() {
		return Name;
	}
	
	public void setName(String name) {
		Name = name;
	}
	
	//image
	
	public String getImage() {
		return Image;
	}
	
	public void setImage(String image) {
		Image = image;
	}

}
