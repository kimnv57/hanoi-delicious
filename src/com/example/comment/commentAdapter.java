package com.example.comment;



import java.io.InputStream;
import java.util.ArrayList;



import com.example.hanoi_delicious2.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class commentAdapter extends ArrayAdapter<comment>{
	ArrayList<comment> itemList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;

	public commentAdapter(Context context, int resource, ArrayList<comment> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		itemList = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.contentview = (TextView) v.findViewById(R.id.TextContentcm);
			holder.nameview = (TextView) v.findViewById(R.id.TextNamecm);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.nameview.setText(itemList.get(position).getName());
		holder.contentview.setText(itemList.get(position).getContent());
		return v;
	}
	
	
	static class ViewHolder {
		public TextView nameview;
		public TextView contentview;

	}

}

