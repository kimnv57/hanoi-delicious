package com.example.hanoi_delicious2;

import variables.MyColor;
import android.R.color;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Window;
import android.widget.TabHost;

import com.example.all_tab.tabMain;
import com.example.all_tab.tabPhotograph;
import com.example.all_tab.tabSetting;
import com.example.all_tab.tabSuggest;
import com.example.all_tab.tabUser;

public class MainActivity extends TabActivity {

	ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		tabUser.session = new SessionManager(getApplicationContext());
		loadTabs();
		// final ActionBar bar = getActionBar();

	}

	public void loadHot() {

	}

	public void loadTabs() {

		// get tabhost
		final TabHost tab = (TabHost) findViewById(android.R.id.tabhost);

		// call setup
		tab.setup();
		TabHost.TabSpec spec;

		// create suggest tab
		spec = tab.newTabSpec("T1");
		spec.setIndicator("",
				getResources().getDrawable(R.drawable.suggest_icon));
		Intent nextIntent = new Intent(this, tabSuggest.class);
		spec.setContent(nextIntent);
		tab.addTab(spec);

		// create photograph tab
		spec = tab.newTabSpec("T2");
		spec.setIndicator("",
				getResources().getDrawable(R.drawable.photograph_icon));
		nextIntent = new Intent(this, tabPhotograph.class);
		spec.setContent(nextIntent);
		tab.addTab(spec);
		// create main tab ( search)
		spec = tab.newTabSpec("T3");
		spec.setIndicator("", getResources().getDrawable(R.drawable.home_icon));
		nextIntent = new Intent(this, tabMain.class);
		spec.setContent(nextIntent);
		tab.addTab(spec);

		// create user tab
		spec = tab.newTabSpec("T4");
		spec.setIndicator("", getResources().getDrawable(R.drawable.user_icon));
		nextIntent = new Intent(this, tabUser.class);
		spec.setContent(nextIntent);
		tab.addTab(spec);

		// create setting tab
		spec = tab.newTabSpec("T5");
		spec.setIndicator("", getResources().getDrawable(R.drawable.ic_setting));
		nextIntent = new Intent(this, tabSetting.class);
		spec.setContent(nextIntent);
		tab.addTab(spec);
		for (int i = 0; i < tab.getTabWidget().getChildCount(); i++) {
			tab.getTabWidget().getChildAt(i).setBackgroundColor(MyColor.BLACK);
			tab.getTabWidget().getChildAt(i).setPadding(0, 0, 0, 0);
		}
		/*
		 * tab.getTabWidget().getChildAt(2).setOnTouchListener(new
		 * OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View v, MotionEvent event) { String
		 * currentSelectedTag = tab.getCurrentTabTag();
		 * //Toast.makeText(getApplicationContext(), currentSelectedTag,
		 * Toast.LENGTH_LONG).show();
		 * if(currentSelectedTag.equalsIgnoreCase("T3")){
		 * //Toast.makeText(getApplicationContext(), "click",
		 * Toast.LENGTH_LONG).show(); //Toast.makeText(getApplicationContext(),
		 * v., Toast.LENGTH_LONG).show(); //
		 * tab.getChildAt(2).setContentView(R.layout.main_tab); Intent
		 * nextIntent = new Intent(); //
		 * tab.getCurrentTab().setContent(nextIntent); } else {
		 * tab.setCurrentTab(2); } return true; } });
		 */

		// set default tab
		tab.setCurrentTab(2);
		tab.getTabWidget().getChildAt(2).setBackgroundColor(Color.BLACK);
		// tab.getTabWidget().setVisibility(View.GONE);

		tab.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			public void onTabChanged(String arg0) {
				String s = "Tab tag =" + arg0 + "; index ="
						+ tab.getCurrentTab();
				for (int i = 0; i < tab.getTabWidget().getChildCount(); i++) {
					tab.getTabWidget().getChildAt(i).setBackgroundColor(MyColor.BLACK);
				}
				tab.getTabWidget().getChildAt(tab.getCurrentTab()).setBackgroundColor(Color.BLACK);

			}
		});

	}
}
