package com.example.hanoi_delicious2;

import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import variables.NameSpace;

import com.example.adapter.LowItem;
import com.example.adapter.LowItemAdapter;
import com.example.all_tab.noFound;

import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class ListFind extends Fragment {
	// thong so webservice
	String METHOD_NAME;
	String SOAP_ACTION;
	// layout

	View rootView;
	TextView mytexttitle;
	TextView mytexttitle2;
	boolean inMain = true;
	GridView gv;
	LowItemAdapter adapter;
	ArrayList<LowItem> itemList;
	ArrayList<String> rank;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.list_search, container, false);
		getview();
		return rootView;
	}

	private void getview() {

		// Adapter cho GridView
		itemList = new ArrayList<LowItem>();
		rank = new ArrayList<String>();
		gv = (GridView) rootView.findViewById(R.id.gridViewSearch);
		mytexttitle = (TextView) rootView.findViewById(R.id.textlist1);
		mytexttitle2 = (TextView) rootView.findViewById(R.id.textlist2);
		if (!NameSpace.SearchPlace.equals("")
				&& !NameSpace.SearchName.equals("")) {
			mytexttitle.setText("Kết quả tìm kiếm cho món ăn : "
					+ NameSpace.SearchName);
			mytexttitle2.setText("Địa điểm: " + NameSpace.SearchPlace);
		}
		if (NameSpace.SearchName.equals("")
				&& !NameSpace.SearchPlace.equals("")) {
			mytexttitle.setText("Kết quả tìm kiếm cho địa điểm : "
					+ NameSpace.SearchPlace);
			mytexttitle2.setVisibility(View.GONE);
		} else {
			if (NameSpace.SearchPlace.equals("")
					&& !NameSpace.SearchName.equals("")) {
				mytexttitle.setText("Kết quả tìm kiếm cho món ăn : "
						+ NameSpace.SearchName);
				mytexttitle2.setVisibility(View.GONE);
			}

		}
		// thiết lập Datasource cho Adapter
		getAdapter();
		adapter = new LowItemAdapter(getActivity().getApplicationContext(),
				R.layout.low_item, itemList);
		// gán Adapter vào Gridview
		gv.setAdapter(adapter);
		// listener to open monan detail
		gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				int index = Integer.parseInt(rank.get(position));
				/*
				 * Intent nextScreen = new Intent(getApplicationContext(),
				 * DetailMain.class); nextScreen.putExtra("id1", index);
				 * startActivity(nextScreen);
				 */
				METHOD_NAME = "getMonan";
				SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
				inMain = false;
				// set detail main
				NameSpace.setId(index);
				setFragmet();
			}
		});

	}

	public void setFragmet() {
		FragmentManager manager = getFragmentManager();
		FragmentTransaction transition = manager.beginTransaction();
		DetailMain frangment = new DetailMain();
		transition.replace(R.id.mainTab1, frangment);
		transition.addToBackStack(null);
		transition.commit();
	}

	// get data
	private void getAdapter() {
		try {
			METHOD_NAME = "findMonanByInfo";
			SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
			SoapObject request = new SoapObject(NameSpace.NAMESPACE,
					METHOD_NAME);
			request.addProperty("findname", NameSpace.getSearchName() + "");
			request.addProperty("findplace", NameSpace.getSearchPlace() + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			MarshalFloat marshal = new MarshalFloat();
			marshal.register(envelope);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(
					NameSpace.URL);
			androidHttpTransport.call(SOAP_ACTION, envelope);
			// Get Array Catalog into soapArray
			SoapObject soapArray = (SoapObject) envelope.getResponse();
			
		
			for (int i = 0; i < soapArray.getPropertyCount(); i++) {
				// (SoapObject) soapArray.getProperty(i) get item at position i
				SoapObject soapItem = (SoapObject) soapArray.getProperty(i);
				// soapItem.getProperty("CateId") get value of CateId property
				String MonanName = soapItem.getProperty("MonanName").toString();
				String MonanImage = soapItem.getProperty("MonanImage")
						.toString();
				String MonanPlace = soapItem.getProperty("MonanPlace")
						.toString();
				String MonanId = soapItem.getProperty("MonanId").toString();

				LowItem item = new LowItem();
				item.setImage(MonanImage);
				item.setName(MonanName);
				rank.add(MonanId);
				itemList.add(item);
			}
			adapter.notifyDataSetChanged();
		} catch (Exception e) {

		}
	}

}