package com.example.hanoi_delicious2;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import variables.MyColor;
import variables.NameSpace;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.all_tab.tabUser;
import com.example.comment.comment;
import com.example.comment.commentAdapter;

public class DetailMain extends Fragment {

	ProgressDialog mProgressDialog;
	// bien de tao list
	int id;
	// thong so webservice
	String METHOD_NAME;
	String SOAP_ACTION;
	// layout

	View rootView;
	ImageView backButton;
	ImageView MonanImage;
	TextView MonanName;
	TextView MonanPlace;
	TextView MonanRestaurent;
	TextView MonanPrice;
	TextView MonanDescribe;
	TextView MonanPoint;
	TextView textRating1;
	RatingBar ratingBar;
	LinearLayout ghimButton;

	// comment
	ArrayList<comment> itemList;
	commentAdapter adapter;
	ListView listComment;
	Button commentButton;
	TextView commentText;

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.detail_item, container, false);

		// comment
		listComment = (ListView) rootView.findViewById(R.id.listComment);
		commentButton = (Button) rootView.findViewById(R.id.commentButton);
		commentText = (TextView) rootView.findViewById(R.id.commentText);

		// get layout
		backButton = (ImageView) rootView.findViewById(R.id.backbutton);
		ghimButton = (LinearLayout) rootView.findViewById(R.id.ghimButton);
		MonanImage = (ImageView) rootView.findViewById(R.id.ImageView2);
		MonanName = (TextView) rootView.findViewById(R.id.name2);
		MonanPlace = (TextView) rootView.findViewById(R.id.place2);
		MonanDescribe = (TextView) rootView.findViewById(R.id.describe);
		MonanPrice = (TextView) rootView.findViewById(R.id.price);
		MonanRestaurent = (TextView) rootView.findViewById(R.id.nhaHang);
		MonanPoint = (TextView) rootView.findViewById(R.id.point);
		ratingBar = (RatingBar) rootView.findViewById(R.id.ratingBar2);
		textRating1 = (TextView) rootView.findViewById(R.id.ratenum);
		LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
		stars.getDrawable(2).setColorFilter(Color.YELLOW,
				PorterDuff.Mode.SRC_ATOP);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		id = NameSpace.getId();
//		Toast.makeText(getActivity().getApplicationContext(),
//				String.format("%d", id), Toast.LENGTH_LONG).show();
		setListener();
		new CreateDetail().execute();
		loadTab();
		/*
		 * getInfo(); getComment();
		 */
		return rootView;
	}

	private class CreateDetail extends AsyncTask<Void, Void, Void> {

		String monanName, monanImage, monanPlace, monanRestaurent, monanPrice,
				monanDescribe, monanPoint, PointCount;
		boolean error = false;

		@Override
		protected Void doInBackground(Void... params) {
			getInfo();
			
			getListComment();
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(DetailMain.this.getActivity());
			// Set progressdialog title
			mProgressDialog.setTitle("Hà Nội Delicious");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			setInfo();
			setComment();
			mProgressDialog.dismiss();

		}

		private void getListComment() {
			try {
				itemList = new ArrayList<comment>();
				METHOD_NAME = "getListCommentById";
				SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
				SoapObject request = new SoapObject(NameSpace.NAMESPACE,
						METHOD_NAME);
				request.addProperty("id", id);

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				MarshalFloat marshal = new MarshalFloat();
				marshal.register(envelope);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(
						NameSpace.URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject soapArray = (SoapObject) envelope.getResponse();

				for (int i = 0; i < soapArray.getPropertyCount(); i++) {
					// (SoapObject) soapArray.getProperty(i) get item at
					// position i
					SoapObject soapItem = (SoapObject) soapArray.getProperty(i);
					// soapItem.getProperty("CateId") get value of CateId
					// property
					String MonanName = soapItem.getProperty("name").toString();
					String Content = soapItem.getProperty("CmConent")
							.toString();
					comment a = new comment();
					a.setContent(Content);
					a.setName(MonanName);
					itemList.add(0,a);
				}
				error = false;
			} catch (Exception e) {
				error = true;
			}
		}

		private void setComment() {

			if (error)
				Toast.makeText(getActivity().getApplicationContext(), "loi",
						Toast.LENGTH_LONG).show();
			else{
			// Adapter cho GridView
			
			adapter = new commentAdapter(getActivity().getApplicationContext(),
					R.layout.comment_item, itemList);

			// gán Adapter vào Gridview
			listComment.setAdapter(adapter);
			// listener to open monan detail\
			}
		}

		private void getInfo() {
			try {
				METHOD_NAME = "getMonan";
				SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
				SoapObject request = new SoapObject(NameSpace.NAMESPACE,
						METHOD_NAME);
				request.addProperty("id", id);
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				MarshalFloat marshal = new MarshalFloat();
				marshal.register(envelope);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(
						NameSpace.URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject soapItem = (SoapObject) envelope.getResponse();

				monanName = soapItem.getProperty("MonanName").toString();
				monanImage = soapItem.getProperty("MonanImage").toString();
				monanPlace = soapItem.getProperty("MonanPlace").toString();
				monanRestaurent = soapItem.getProperty("resName").toString();
				monanPrice = soapItem.getProperty("MonanPrice").toString();
				monanDescribe = soapItem.getProperty("MonanDescribe")
						.toString();
				monanPoint = soapItem.getProperty("Point").toString();
				PointCount = soapItem.getProperty("PointCount").toString();
				int point = Integer.parseInt(monanPoint);
				float pointf = point;
				pointf = pointf / 10;
				monanPoint = String.format("%.1f", pointf);

			} catch (Exception e) {
				//Toast.makeText(getActivity().getApplicationContext(),
				//		e.getMessage(), Toast.LENGTH_LONG).show();
			}
		}

		private void setInfo() {
			MonanName.setText(monanName);
			MonanDescribe.setText("#Admin : " + monanDescribe);
			MonanPlace.setText(" " + monanPlace);
			MonanPrice.setText(monanPrice);
			MonanRestaurent.setText(monanRestaurent);
			MonanPoint.setText("Đánh Giá: " + monanPoint + " / " + PointCount
					+ "người đánh giá");
			textRating1.setText("" + monanPoint + " Điểm");
			new DownloadImageTask(MonanImage).execute(monanImage);
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

	}

	private class Rating extends AsyncTask<Void, Void, Void> {
		int myrate;
		boolean run = true;
		int kq = 0;

		public Rating(int myrate) {
			this.myrate = myrate;
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				if (!run)
					return null;

				HashMap<String, String> myUser = tabUser.session
						.getUserDetails();
				String name = myUser.get("name");

				METHOD_NAME = "UserPointMonan";
				SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
				SoapObject request = new SoapObject(NameSpace.NAMESPACE,
						METHOD_NAME);
				request.addProperty("monanID", id);
				request.addProperty("username", name);
				request.addProperty("point", myrate);
				// Toast.makeText(getActivity().getApplicationContext(),
				// String.format("%d", myrate), Toast.LENGTH_LONG).show();

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				MarshalFloat marshal = new MarshalFloat();
				marshal.register(envelope);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(
						NameSpace.URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapPrimitive soapResult = (SoapPrimitive) envelope
						.getResponse();
				kq = Integer.parseInt(soapResult.toString());

			} catch (IOException e1) {

			} catch (XmlPullParserException e2) {

			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(DetailMain.this.getActivity());
			// Set progressdialog title
			mProgressDialog.setTitle("Hà Nội Delicious");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
			if (!tabUser.session.isLoggedIn()) {
				alert.showAlertDialog(getActivity(), "Rating failed..",
						"Bạn chưa đăng nhập", false);
				run = false;
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			if (!run) {
				mProgressDialog.dismiss();
				return;
			}
			if (kq == -1) {
				Toast.makeText(getActivity().getApplicationContext(),
						" thất bại ", Toast.LENGTH_LONG).show();
			}
			if (kq == 0) {
				alert.showAlertDialog(getActivity(), "Rating failed..",
						"Bạn đã đánh giá món ăn này rồi", false);
			}
			if (kq == 1) {
				alert.showAlertDialog(getActivity(), "Thành công..",
						"Bạn đã đánh giá " + myrate / 10
								+ " sao cho món ăn này", true);
			}
			mProgressDialog.dismiss();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

	}
	
	
	private class Comment extends AsyncTask<Void, Void, Void> {
		String name, comment;
		boolean error = false;
		public Comment(String name, String comment) {
			this.name=name;
			this.comment=comment;
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				METHOD_NAME = "UserAddComment";
				SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
				SoapObject request = new SoapObject(NameSpace.NAMESPACE,
						METHOD_NAME);
				request.addProperty("monanId", id);
				request.addProperty("username", name);
				request.addProperty("content", comment);
				// Toast.makeText(getActivity().getApplicationContext(),
				// String.format("%d", myrate), Toast.LENGTH_LONG).show();

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				MarshalFloat marshal = new MarshalFloat();
				marshal.register(envelope);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(
						NameSpace.URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapPrimitive soapResult = (SoapPrimitive) envelope
						.getResponse();
				String kq = soapResult.toString();
				if (kq.equals("false")) {
					error=true;
				}else{
					error=false;
				}

			} catch (IOException e1) {
				error=true;

			} catch (XmlPullParserException e2) {
				error=true;
				
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(DetailMain.this.getActivity());
			// Set progressdialog title
			mProgressDialog.setTitle("Hà Nội Delicious");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			
			if (error) {
				alert.showAlertDialog(getActivity(), "Nhận xét thất bại..",
						"server error", false);
			}
			else {
				getListComment();
				adapter.notifyDataSetChanged();
			}
			mProgressDialog.dismiss();
		}
		private void getListComment() {
			try {
				METHOD_NAME = "getListCommentById";
				SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
				SoapObject request = new SoapObject(NameSpace.NAMESPACE,
						METHOD_NAME);
				request.addProperty("id", id);

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				MarshalFloat marshal = new MarshalFloat();
				marshal.register(envelope);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(
						NameSpace.URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapObject soapArray = (SoapObject) envelope.getResponse();
				itemList.clear();
				for (int i = 0; i < soapArray.getPropertyCount(); i++) {
					// (SoapObject) soapArray.getProperty(i) get item at
					// position i
					SoapObject soapItem = (SoapObject) soapArray.getProperty(i);
					// soapItem.getProperty("CateId") get value of CateId
					// property
					String MonanName = soapItem.getProperty("name").toString();
					String Content = soapItem.getProperty("CmConent")
							.toString();
					comment a = new comment();
					a.setContent(Content);
					a.setName(MonanName);
					itemList.add(0,a);
				}
				error = false;
			} catch (Exception e) {
				error = true;
			}
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

	}
	
	private class Ghim extends AsyncTask<Void, Void, Void> {
		String name;
		boolean error = false;
		boolean success = false;
		public Ghim(String name) {
			this.name=name;
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				METHOD_NAME = "UserAddGhim";
				SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
				SoapObject request = new SoapObject(NameSpace.NAMESPACE,
						METHOD_NAME);
				request.addProperty("monanID", id);
				request.addProperty("username", name);
				// Toast.makeText(getActivity().getApplicationContext(),
				// String.format("%d", myrate), Toast.LENGTH_LONG).show();

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				MarshalFloat marshal = new MarshalFloat();
				marshal.register(envelope);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(
						NameSpace.URL);
				androidHttpTransport.call(SOAP_ACTION, envelope);
				SoapPrimitive soapResult = (SoapPrimitive) envelope
						.getResponse();
				String kq = soapResult.toString();
				if (kq.equals("false")) {
					success=false;
				}else{
					success=true;
				}
				error=false;
				
			} catch (IOException e1) {
				error=true;
			} catch (XmlPullParserException e2) {
				error=true;
			}
				
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(DetailMain.this.getActivity());
			// Set progressdialog title
			mProgressDialog.setTitle("Hà Nội Delicious");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			
			if (error) {
				alert.showAlertDialog(getActivity(), "Ghim thất bại..",
						"sever error", false);
			}
			else {
				if(success) {
					alert.showAlertDialog(getActivity(), "Ghim thành công..",
							"Bạn đã ghim món ăn này", true);
				}
				else alert.showAlertDialog(getActivity(), "Ghim thất bại..",
						"Bạn đã Ghim món ăn này rồi", false);
			}
			mProgressDialog.dismiss();
		}

	}
	
	
	

	OnTouchListener focusHandler = new OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			view.requestFocusFromTouch();
			return false;
		}
	};
	private void setListener() {
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
					getActivity().onBackPressed();
				}
		});

		ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				int myrate = (int) ratingBar.getRating() * 10;
				new Rating(myrate).execute();
			}
		});
		commentText.setOnTouchListener(focusHandler);
		commentButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String comment = commentText.getText().toString();
				if (!tabUser.session.isLoggedIn()) {
					alert.showAlertDialog(getActivity(), "Comment error",
							"Bạn chưa đăng nhập", false);
					return;
				}
				if (comment.trim().length() == 0) {
					alert.showAlertDialog(getActivity(), "Comment error",
							"Vui lòng nhập đầy đủ", false);
					return;
				}
				HashMap<String, String> myUser = tabUser.session
						.getUserDetails();
				String name = myUser.get("name");
				new Comment(name,comment).execute();
				
			}
		});
		ghimButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				
					if (!tabUser.session.isLoggedIn()) {
						alert.showAlertDialog(getActivity(), "Ghim thất bại..",
								"Bạn chưa đăng nhập", false);
						return;
					}

					HashMap<String, String> myUser = tabUser.session
							.getUserDetails();
					String name = myUser.get("name");
					new Ghim(name).execute();
					
			}
		});

	}

	private void loadTab() {

		final TextView title[] = new TextView[2];
		final TextView line[] = new TextView[2];
		final TabHost tab = (TabHost) rootView.findViewById(R.id.tabHostDetail);
		// gọi lệnh setup
		tab.setup();
		TabHost.TabSpec spec;
		// Tạo tab1
		spec = tab.newTabSpec("t1");
		spec.setContent(R.id.tab1);
		View tabIndicator = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_widget_main, tab.getTabWidget(), false);
		title[0] = (TextView) tabIndicator.findViewById(R.id.nameWidget);
		line[0] = (TextView) tabIndicator.findViewById(R.id.lineWidget);
		title[0].setText("Thông tin");
		title[0].setTextSize(15);
		title[0].setTextColor(MyColor.STRONGORAGE);
		title[0].setBackgroundColor(MyColor.MYQUITEGREY);
		line[0].setBackgroundColor(MyColor.STRONGORAGE);
		spec.setIndicator(tabIndicator);
		tab.addTab(spec);
		// Tạo tab2
		spec = tab.newTabSpec("t2");
		spec.setContent(R.id.tab2);
		View tabIndicator2 = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_widget_main, tab.getTabWidget(), false);
		title[1] = (TextView) tabIndicator2.findViewById(R.id.nameWidget);
		line[1] = (TextView) tabIndicator2.findViewById(R.id.lineWidget);
		title[1].setText("Nhận xét");
		title[1].setTextSize(15);
		title[1].setTextColor(MyColor.BLACK);
		title[1].setBackgroundColor(MyColor.MYQUITEGREY);
		line[1].setBackgroundColor(MyColor.MYQUITEGREY);
		spec.setIndicator(tabIndicator2);
		tab.addTab(spec);
		tab.setCurrentTab(0);
		tab.getTabWidget().getChildAt(0).setPadding(0, 0, 0, 0);
		tab.getTabWidget().getChildAt(1).setPadding(0, 0, 0, 0);

		tab.getTabWidget().getChildAt(0)
				.setBackgroundColor(MyColor.MYQUITEGREY);
		tab.getTabWidget().getChildAt(1).setBackgroundColor(MyColor.MYGREY);
		tab.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			public void onTabChanged(String arg0) {
				for (int i = 0; i < 2; i++) {
					title[i].setBackgroundColor(MyColor.MYGREY);
					line[i].setBackgroundColor(MyColor.MYGREY);
					title[i].setTextColor(MyColor.BLACK);
				}
				title[tab.getCurrentTab()].setTextColor(MyColor.STRONGORAGE);
				title[tab.getCurrentTab()]
						.setBackgroundColor(MyColor.MYQUITEGREY);
				line[tab.getCurrentTab()]
						.setBackgroundColor(MyColor.STRONGORAGE);
			}
		});
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// progressBar.setVisibility(ProgressBar.VISIBLE);
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onCancelled(Bitmap result) {
			// TODO Auto-generated method stub
			super.onCancelled(result);
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}

	}

}
