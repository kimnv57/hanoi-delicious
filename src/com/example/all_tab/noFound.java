package com.example.all_tab;

import com.example.hanoi_delicious2.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class noFound extends Fragment{

	public noFound() {
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.no_found, container, false);
		return rootView;
	}

}
