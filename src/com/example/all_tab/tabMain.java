package com.example.all_tab;

import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import variables.MyColor;
import variables.NameSpace;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adapter.LowItem;
import com.example.adapter.LowItemAdapter;
import com.example.hanoi_delicious2.DetailMain;
import com.example.hanoi_delicious2.ListFind;
import com.example.hanoi_delicious2.R;

public class tabMain extends Activity {

	protected final int COUNT_PER_PAGE = 10;
	protected int totalHot, totalNew;
	protected int startLoadHot = 0;
	protected int startLoadNew = 0;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] mPlanetTitles;

	String arr[] = { "All", "Quận Ba Đình", "Quận Hoàn Kiếm", "Quận Tây Hồ",
			"Quận Long Biên", "Quận Cầu Giấy", "Quận Đống Đa",
			"Quận Hai Bà Trưng", "Quận Hoàng Mai", "Quận Thanh Xuân",
			"Quận Hà Đông", "Quận Bắc Từ Liêm", "Quận Nam Từ Liêm" };

	ProgressDialog mProgressDialog;
	boolean inMain = true;
	LowItemAdapter adapterHot;
	LowItemAdapter adapterNew;
	String METHOD_NAME;
	String SOAP_ACTION;
	ArrayList<LowItem> itemListHot;
	ArrayList<LowItem> itemListNew;
	ArrayList<String> rankHot;
	ArrayList<String> rankNew;
	EditText searchName;
	TextView searchPlace;
	
	RelativeLayout loading;
	int check = -1; // keo dan search

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main_tab);
		// cai dat de vao mang
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		loadTabs();
		setlisten();
		

		new LoadInfo().execute();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void onBackPressed() {
		Toast.makeText(getApplicationContext(), "back", Toast.LENGTH_LONG)
				.show();
		super.onBackPressed();
	}

	private class LoadInfo extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			rankNew = new ArrayList<String>();
			itemListNew = new ArrayList<LowItem>();
			itemListHot = new ArrayList<LowItem>();
			rankHot = new ArrayList<String>();
			loadHot();
			loadNew();
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			loading = (RelativeLayout) findViewById(R.id.tabloading);
			loading.setVisibility(View.VISIBLE);
			final GifView a= (GifView) findViewById(R.id.imageloading);
			a.loadGIFResource(getApplicationContext(), R.drawable.loader2);
			a.setXY(60, 220);
			
			// Create a progressdialog
//			mProgressDialog = new ProgressDialog(tabMain.this);
//			// Set progressdialog title
//			mProgressDialog.setTitle("Hà Nội Delicious");
//			// Set progressdialog message
//			mProgressDialog.setMessage("Loading...");
//			mProgressDialog.setIndeterminate(false);
//			// Show progressdialog
//			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			
			setHot();
			setNew();
			loading.setVisibility(View.GONE);
			//mProgressDialog.dismiss();
		}

	}
	
	
	private class LoadMoreHot extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			loadHot();
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(tabMain.this);
			// Set progressdialog title
			mProgressDialog.setTitle("Hà Nội Delicious");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading more.");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			setHot();
			mProgressDialog.dismiss();
		}

	}
	
	private class LoadMoreNew extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			loadNew();
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(tabMain.this);
			// Set progressdialog title
			mProgressDialog.setTitle("Hà Nội Delicious");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading more.");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			setNew();
			mProgressDialog.dismiss();
		}

	}

	// search

	private void setlisten() {

		LinearLayout placebutton = (LinearLayout) findViewById(R.id.placebutton);
		placebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDrawerLayout.openDrawer(Gravity.LEFT);
			}
		});
		searchName = (EditText) findViewById(R.id.searchText);
		searchPlace = (TextView) findViewById(R.id.searchPlace);
		searchPlace.setText("All");
		OnTouchListener focusHandler = new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				view.requestFocusFromTouch();
				view.requestFocus();
				int a = ((View) view.getParent()).getWidth();
				if (check == -1) {
					// ((View) view.getParent()).setLayoutParams(new
					// LayoutParams(a+50,60));
					check = 0;
				}
				return false;
			}
		};
		searchName.setOnTouchListener(focusHandler);
		searchName.setSelected(false);
		// searchPlace.setOnTouchListener(focusHandler);
		searchPlace.setSelected(false);
		searchName.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					inMain = false;
					String monAnName = searchName.getText().toString();
					String monAnPlace = searchPlace.getText().toString();
					if (monAnPlace.equals("All"))
						monAnPlace = "";
					NameSpace.setSearchName(monAnName);
					NameSpace.setSearchPlace(monAnPlace);
					FragmentManager manager = getFragmentManager();
					FragmentTransaction transition = manager.beginTransaction();
					ListFind frangment = new ListFind();
					transition.replace(android.R.id.tabhost, frangment);
					transition.addToBackStack(null);
					transition.commit();
					return true;
				}
				return false;
			}
		});
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				mDrawerList.setItemChecked(position, true);
				searchPlace.setText(arr[position]);
				mDrawerLayout.closeDrawer(mDrawerList);

			}
		});
		searchName.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (check == 0 && hasFocus) {
					int a = ((View) v.getParent()).getWidth();
					// ((View) v.getParent()).setLayoutParams(new
					// LayoutParams(a-50,60));
					check = -1;
				}
			}
		});

	}

	public void setFragmet() {
		FragmentManager manager = getFragmentManager();
		FragmentTransaction transition = manager.beginTransaction();
		DetailMain frangment = new DetailMain();
		inMain = false;
		transition.replace(R.id.mainTab1, frangment);
		transition.addToBackStack(null);
		transition.commit();
	}

	@Override
	protected void onPause() {
		super.onPause();
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		// setContentView(R.layout.main_tab);
	}

	// nut back main
	/*
	 * @Override
	 * 
	 * public void onBackPressed() { if (inMain) super.onBackPressed(); else {
	 * // loadTabs(); setContentView(R.layout.main_tab); loadTabs();
	 * setlisten(); loadHot(); loadNew(); inMain = true; } return; }
	 */

	// load tab main
	public void loadTabs() {

		final TabHost tab = (TabHost) findViewById(android.R.id.tabhost);
		final TabHost tab1 = (TabHost) this.getParent().findViewById(
				android.R.id.tabhost);
		final TextView title[] = new TextView[3];
		final TextView line[] = new TextView[3];

		mPlanetTitles = arr;
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.right_drawer);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, mPlanetTitles));

		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.app_name, /* "open drawer" description for accessibility */
		R.string.hello_world /* "close drawer" description for accessibility */
		) {
			public void onDrawerClosed(View view) {
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		// call setup
		tab.setup();
		TabHost.TabSpec spec;

		spec = tab.newTabSpec("t1");
		spec.setContent(R.id.tabHot);
		View tabIndicator = LayoutInflater.from(this).inflate(
				R.layout.tab_widget_main, tab.getTabWidget(), false);
		title[0] = (TextView) tabIndicator.findViewById(R.id.nameWidget);
		line[0] = (TextView) tabIndicator.findViewById(R.id.lineWidget);
		title[0].setText("Nổi bật");
		title[0].setTextSize(15);
		title[0].setTextColor(MyColor.STRONGORAGE);
		title[0].setBackgroundColor(MyColor.MYQUITEGREY);
		line[0].setBackgroundColor(MyColor.STRONGORAGE);
		spec.setIndicator(tabIndicator);
		tab.addTab(spec);

		// create new tab
		spec = tab.newTabSpec("t2");
		spec.setContent(R.id.tabNew);
		View tabIndicator1 = LayoutInflater.from(this).inflate(
				R.layout.tab_widget_main, tab.getTabWidget(), false);
		title[1] = (TextView) tabIndicator1.findViewById(R.id.nameWidget);
		line[1] = (TextView) tabIndicator1.findViewById(R.id.lineWidget);
		title[1].setText("Mới ra");
		title[1].setTextSize(15);
		title[1].setTextColor(MyColor.BLACK);
		title[1].setBackgroundColor(MyColor.MYGREY);
		line[1].setBackgroundColor(MyColor.MYGREY);
		spec.setIndicator(tabIndicator1);
		tab.addTab(spec);

		// create place tab
		spec = tab.newTabSpec("t2");
		spec.setContent(R.id.tabPlace);
		View tabIndicator2 = LayoutInflater.from(this).inflate(
				R.layout.tab_widget_main, tab.getTabWidget(), false);
		title[2] = (TextView) tabIndicator2.findViewById(R.id.nameWidget);
		line[2] = (TextView) tabIndicator2.findViewById(R.id.lineWidget);
		title[2].setText("Gần đây");
		title[2].setTextSize(15);
		title[2].setTextColor(MyColor.BLACK);
		title[2].setBackgroundColor(MyColor.MYGREY);
		line[2].setBackgroundColor(MyColor.MYGREY);
		spec.setIndicator(tabIndicator2);
		tab.addTab(spec);

		// set current tab
		tab.setCurrentTab(0);
		tab.getTabWidget().getChildAt(0).setSelected(true);
		tab.getTabWidget().setStripEnabled(false);
		tab.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			public void onTabChanged(String arg0) {
				for (int i = 0; i < 3; i++) {
					title[i].setBackgroundColor(MyColor.MYGREY);
					line[i].setBackgroundColor(MyColor.MYGREY);
					title[i].setTextColor(MyColor.BLACK);
				}
				title[tab.getCurrentTab()]
						.setTextColor(MyColor.STRONGORAGE);
				title[tab.getCurrentTab()]
						.setBackgroundColor(MyColor.MYQUITEGREY);
				line[tab.getCurrentTab()]
						.setBackgroundColor(MyColor.STRONGORAGE);
			}
		});
		tab1.getTabWidget().getChildAt(2)
				.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						String currentSelectedTag = tab1.getCurrentTabTag();
						if (currentSelectedTag.equalsIgnoreCase("T3")
								&& !inMain) {
							inMain = true;
							setContentView(R.layout.main_tab);
							loadTabs();
							startLoadHot=startLoadNew=0;
							new LoadInfo().execute();
							setlisten();

						} else {
							tab1.setCurrentTab(2);
						}
						return true;
					}
				});
	}

	// load mon an hot
	public void loadHot() {
		METHOD_NAME = "getListMonanHot";
		SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;

		// Adapter cho GridView
		

		// thiết lập Datasource cho Adapter
		getAdapter(0);
		adapterHot = new LowItemAdapter(getApplicationContext(),
				R.layout.low_item, itemListHot);
		// gán Adapter vào Gridview

	}

	// load and set UI mon an moi
	public void loadNew() {
		METHOD_NAME = "getListMonanMoi";
		SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
		

		// thiết lập Datasource cho Adapter
		getAdapter(1);
		adapterNew = new LowItemAdapter(getApplicationContext(),
				R.layout.low_item, itemListNew);
		// set Adapter for Gridview

	}

	public void setHot() {
		GridView gv;
		gv = (GridView) findViewById(R.id.gridViewHot);
		gv.setAdapter(adapterHot);
		// listener to open monan detail
		gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				int index = Integer.parseInt(rankHot.get(position));
				/*
				 * Intent nextScreen = new Intent(getApplicationContext(),
				 * DetailMain.class); nextScreen.putExtra("id1", index);
				 * startActivity(nextScreen);
				 */
				METHOD_NAME = "getMonan";
				SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
				inMain = false;
				// set detail main
				NameSpace.setId(index);
				setFragmet();
			}
		});
		gv.setOnScrollListener(new OnScrollListener() {
			int totalitem;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (scrollState == SCROLL_STATE_FLING) {
					if (totalHot > totalitem) {

						int first = view.getFirstVisiblePosition();
						int count = view.getChildCount();
						Log.d("grid", first + " " + count);
						if (first == totalitem - 6 || first == totalitem - 5) {
							Log.d("grid", "dang load ");
							new LoadMoreHot().execute();
						}
					} else {
						Log.d("gridview", "khong load nua");
					}
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				totalitem = totalItemCount;
				if (firstVisibleItem == totalItemCount - 6
						|| firstVisibleItem == totalItemCount - 5) {
					// Toast.makeText(getApplicationContext(), "load thoi",
					// Toast.LENGTH_LONG).show();
				}

			}
		});
	}

	public void setNew() {
		GridView gv;
		gv = (GridView) findViewById(R.id.gridViewNew);
		gv.setAdapter(adapterNew);
		//
		gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				int index = Integer.parseInt(rankNew.get(position));
				Log.d("kho", "index=" + index);
				METHOD_NAME = "getMonan";
				SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
				inMain = false;
				NameSpace.setId(index);
				// set view
				id = Integer.parseInt(rankHot.get(position));
				setFragmet();
			}
		});
		
		gv.setOnScrollListener(new OnScrollListener() {
			int totalitem;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (scrollState == SCROLL_STATE_FLING) {
					if (totalNew > totalitem) {

						int first = view.getFirstVisiblePosition();
						int count = view.getChildCount();
						Log.d("grid", first + " " + count);
						if (first == totalitem - 6 || first == totalitem - 5) {
							Log.d("grid", "dang load ");
							new LoadMoreNew().execute();
						}
					} else {
						Log.d("gridview", "khong load nua");
					}
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				totalitem = totalItemCount;
				if (firstVisibleItem == totalItemCount - 6
						|| firstVisibleItem == totalItemCount - 5) {
					// Toast.makeText(getApplicationContext(), "load thoi",
					// Toast.LENGTH_LONG).show();
				}

			}
		});
	}

	// get data
	private void getAdapter(int check) {
		try {
			SoapObject request = new SoapObject(NameSpace.NAMESPACE,
					METHOD_NAME);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			MarshalFloat marshal = new MarshalFloat();
			marshal.register(envelope);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(
					NameSpace.URL);
			androidHttpTransport.call(SOAP_ACTION, envelope);
			// Get Array Catalog into soapArray
			SoapObject soapArray = (SoapObject) envelope.getResponse();
			// arrCate.clear();
			// soapArray.getPropertyCount() return number of
			// element in soapArray
			if (check == 0) {
				totalHot = soapArray.getPropertyCount();
				int max;
				max=totalHot-startLoadHot;
				if(max>COUNT_PER_PAGE) max=COUNT_PER_PAGE;
				for (int i = startLoadHot; i < max+startLoadHot; i++) {
					// (SoapObject) soapArray.getProperty(i) get item at
					// position i
					SoapObject soapItem = (SoapObject) soapArray.getProperty(i);
					// soapItem.getProperty("CateId") get value of CateId
					// property
					String MonanName = soapItem.getProperty("MonanName")
							.toString();
					String MonanImage = soapItem.getProperty("MonanImage")
							.toString();
					String MonanPlace = soapItem.getProperty("MonanPlace")
							.toString();
					String MonanId = soapItem.getProperty("MonanId").toString();
					LowItem item = new LowItem();
					item.setImage(MonanImage);
					item.setName(MonanName);
					rankHot.add(MonanId);
					itemListHot.add(item);
				}
				startLoadHot+=COUNT_PER_PAGE;
			}
			if (check == 1) {
				totalNew = soapArray.getPropertyCount();
				int max;
				max=totalNew-startLoadNew;
				if(max>COUNT_PER_PAGE) max=COUNT_PER_PAGE;
				for (int i = startLoadNew; i < max+startLoadNew; i++) {
					// (SoapObject) soapArray.getProperty(i) get item at
					// position i
					SoapObject soapItem = (SoapObject) soapArray.getProperty(i);
					// soapItem.getProperty("CateId") get value of CateId
					// property
					String MonanName = soapItem.getProperty("MonanName")
							.toString();
					String MonanImage = soapItem.getProperty("MonanImage")
							.toString();
					String MonanPlace = soapItem.getProperty("MonanPlace")
							.toString();
					String MonanId = soapItem.getProperty("MonanId").toString();

					LowItem item = new LowItem();
					item.setImage(MonanImage);
					item.setName(MonanName);
					rankNew.add(MonanId);
					itemListNew.add(item);
					Log.v("hihihihi", "index=" + MonanId);
				}
				startLoadNew+=COUNT_PER_PAGE;
			}
		} catch (Exception e) {
		}
	}

	// Khởi tạo các đối tượng và gán ADapter cho ListView
	public void doFormWidgets() {
	}

}