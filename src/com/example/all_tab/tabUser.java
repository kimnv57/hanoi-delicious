package com.example.all_tab;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import variables.MyColor;
import variables.NameSpace;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adapter.LowItem;
import com.example.adapter.LowItemAdapter;
import com.example.hanoi_delicious2.AlertDialogManager;
import com.example.hanoi_delicious2.DetailMain;
import com.example.hanoi_delicious2.R;
import com.example.hanoi_delicious2.SessionManager;

public class tabUser extends Activity {
	boolean inMain = true;
	EditText userName, userPassword, userNameReg, passwordReg, userPlaceReg,
			userFullnameReg;
	// thong so webservice
	String METHOD_NAME;
	String SOAP_ACTION;

	LowItemAdapter adapter;
	ArrayList<LowItem> itemList;
	ArrayList<String> rank;

	// login button
	Button btnLogin;
	// resign button
	Button btnResign;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Session Manager Class
	public static SessionManager session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Session Manager
		session = new SessionManager(getApplicationContext());
		if (session.isLoggedIn()) {
			setContentView(R.layout.personal_page);
			HashMap<String, String> mystring = session.getUserDetails();
			String username = mystring.get("name");
			String place = mystring.get("place");
			setUserInfo(username, place);
			new LoadMonanByUserName(username).execute();

		} else {
			setContentView(R.layout.user_tab);
			loadTabs();
			setLoginListener();
			setResignListener();
		}

		// setting network
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
	}
	
	

	private void setResignListener() {
		userNameReg = (EditText) findViewById(R.id.regUsername);
		passwordReg = (EditText) findViewById(R.id.reg_password);
		userPlaceReg = (EditText) findViewById(R.id.reg_place);
		userFullnameReg = (EditText) findViewById(R.id.reg_fullname);

		btnResign = (Button) findViewById(R.id.btnRegister);

		userNameReg.setOnTouchListener(focusHandler);
		passwordReg.setOnTouchListener(focusHandler);
		userPlaceReg.setOnTouchListener(focusHandler);
		userFullnameReg.setOnTouchListener(focusHandler);
		btnResign.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "chay",
						Toast.LENGTH_LONG).show();
				String uname = userNameReg.getText().toString();
				String ufullname = userFullnameReg.getText().toString();
				String uplace = userPlaceReg.getText().toString();
				String upassord = passwordReg.getText().toString();
				if (!(uname.trim().length() > 0
						&& ufullname.trim().length() > 0
						&& uplace.trim().length() > 0 && upassord.trim()
						.length() > 0)) {
					alert.showAlertDialog(tabUser.this,
							"Dang ki khong thanh cong",
							"vui long nhap day du thong tin", false);
					return;
				}
				addUserToNetWork();
			}

		});

	}

	// send request add user
	private void addUserToNetWork() {

		try {
			METHOD_NAME = "addUser";
			SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;

			SoapObject request = new SoapObject(NameSpace.NAMESPACE,
					METHOD_NAME);
			SoapObject u = new SoapObject(NameSpace.NAMESPACE, "u");
			u.addProperty("UserName", userNameReg.getText() + "");
			u.addProperty("UserFullName", userFullnameReg.getText().toString()
					+ "");
			u.addProperty("UserPlace", userPlaceReg.getText() + "");
			u.addProperty("UserPassword", passwordReg.getText() + "");
			u.addProperty("UserImage", "");
			request.addSoapObject(u);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE androidHttpTransport = new HttpTransportSE(
					NameSpace.URL);
			androidHttpTransport.call(SOAP_ACTION, envelope);

			SoapPrimitive soapPrimitive = (SoapPrimitive) envelope
					.getResponse();

			int ret = Integer.parseInt(soapPrimitive.toString());

			if (ret == -1)
				alert.showAlertDialog(tabUser.this, "Đăng ký không thành công",
						"Tên tài khoản đã có", false);
			else {
				alert.showAlertDialog(tabUser.this, "Xin chào",
						"Đăng ký tài khoản thành công", true);
				TabHost tab = (TabHost) findViewById(android.R.id.tabhost);
				tab.setCurrentTab(0);
				userName = (EditText) findViewById(R.id.userName);
				userPassword = (EditText) findViewById(R.id.userPassword);
				userName.setText(userNameReg.getText());
				userPassword.setText(passwordReg.getText());
			}
		} catch (Exception e) {

		}

	}

	OnTouchListener focusHandler = new OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			view.requestFocusFromTouch();
			return false;
		}
	};

	private void setLoginListener() {
		userName = (EditText) findViewById(R.id.userName);
		userPassword = (EditText) findViewById(R.id.userPassword);
		userName.setOnTouchListener(focusHandler);
		userPassword.setOnTouchListener(focusHandler);
		Button dangNhap = (Button) findViewById(R.id.btnDnhap);
		userPassword.setActivated(true);
		userPassword.setTextIsSelectable(true);
		dangNhap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String username = userName.getText().toString();
				String password = userPassword.getText().toString();
				if (username.trim().length() > 0
						&& password.trim().length() > 0) {
					if (userAndPasswordOk(username, password)) {
						String place = getUserPlace(username);
						setContentView(R.layout.personal_page);
						session.createLoginSession(username, place);
						setUserInfo(username, place);
						new LoadMonanByUserName(username).execute();

					} else {
						// username / password doesn't match
						alert.showAlertDialog(tabUser.this, "Login failed..",
								"Username/Password is incorrect", false);
					}
				} else {
					// user didn't entered username or password
					alert.showAlertDialog(tabUser.this, "Login failed..",
							"Please enter username and password", false);
				}

			}

		});
	}

	// setting user page
	private void setUserInfo(String username, String place) {
		TextView nameText = (TextView) findViewById(R.id.userName);
		TextView placeText = (TextView) findViewById(R.id.userPlace);
		Button logout = (Button) findViewById(R.id.LogoutButton);
		nameText.setText(username);
		placeText.setText(place);
		
		final TabHost tab1 = (TabHost) this.getParent().findViewById(
				android.R.id.tabhost);
		tab1.getTabWidget().getChildAt(3)
		.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				String currentSelectedTag = tab1.getCurrentTabTag();
				if (currentSelectedTag.equalsIgnoreCase("T4")
						&& !inMain) {
					inMain=true;
					setContentView(R.layout.personal_page);
					HashMap<String, String> mystring = session.getUserDetails();
					String username = mystring.get("name");
					String place = mystring.get("place");
					setUserInfo(username, place);
					new LoadMonanByUserName(username).execute();

				} else {
					tab1.setCurrentTab(3);
				}
				return true;
			}
		});
		logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				session.logoutUser();
				setContentView(R.layout.user_tab);
				loadTabs();
				setLoginListener();
				setResignListener();
			}
		});
	}

	private void loadTabs() {
		final TextView title[] = new TextView[2];
		final TextView line[] = new TextView[2];
		final TabHost tab = (TabHost) findViewById(android.R.id.tabhost);
		
		// gọi lệnh setup
		tab.setup();
		TabHost.TabSpec spec;
		// Tạo tab1
		spec = tab.newTabSpec("t1");
		spec.setContent(R.id.tabLogin);
		View tabIndicator = LayoutInflater.from(this).inflate(
				R.layout.tab_widget_main, tab.getTabWidget(), false);
		title[0] = (TextView) tabIndicator.findViewById(R.id.nameWidget);
		line[0] = (TextView) tabIndicator.findViewById(R.id.lineWidget);
		title[0].setText("Đăng nhập");
		title[0].setTextSize(15);
		title[0].setTextColor(MyColor.STRONGORAGE);
		title[0].setBackgroundColor(MyColor.MYQUITEGREY);
		line[0].setBackgroundColor(MyColor.STRONGORAGE);
		spec.setIndicator(tabIndicator);
		tab.addTab(spec);
		// Tạo tab2
		spec = tab.newTabSpec("t2");
		spec.setContent(R.id.tabResign);
		View tabIndicator2 = LayoutInflater.from(this).inflate(
				R.layout.tab_widget_main, tab.getTabWidget(), false);
		title[1] = (TextView) tabIndicator2.findViewById(R.id.nameWidget);
		line[1] = (TextView) tabIndicator2.findViewById(R.id.lineWidget);
		title[1].setText("Đăng kí");
		title[1].setTextSize(15);
		title[1].setTextColor(MyColor.BLACK);
		title[1].setBackgroundColor(MyColor.MYQUITEGREY);
		line[1].setBackgroundColor(MyColor.MYQUITEGREY);
		spec.setIndicator(tabIndicator2);
		tab.addTab(spec);
		tab.setCurrentTab(0);
		tab.getTabWidget().getChildAt(0).setPadding(0, 0, 0, 0);
		tab.getTabWidget().getChildAt(1).setPadding(0, 0, 0, 0);

		tab.getTabWidget().getChildAt(0)
				.setBackgroundColor(MyColor.MYQUITEGREY);
		tab.getTabWidget().getChildAt(1).setBackgroundColor(MyColor.MYGREY);
		tab.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			public void onTabChanged(String arg0) {
				for (int i = 0; i < 2; i++) {
					title[i].setBackgroundColor(MyColor.MYGREY);
					line[i].setBackgroundColor(MyColor.MYGREY);
					title[i].setTextColor(MyColor.BLACK);
				}
				title[tab.getCurrentTab()].setTextColor(MyColor.STRONGORAGE);
				title[tab.getCurrentTab()]
						.setBackgroundColor(MyColor.MYQUITEGREY);
				line[tab.getCurrentTab()]
						.setBackgroundColor(MyColor.STRONGORAGE);
			}
		});
		
	}

	// kiem tra xem tai khoan va mat khau co dung chua.
	public boolean userAndPasswordOk(String userName, String password) {
		METHOD_NAME = "haveUserNameWithPassword";
		SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
		SoapObject request = new SoapObject(NameSpace.NAMESPACE, METHOD_NAME);
		request.addProperty("name", userName);
		request.addProperty("password", password);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		MarshalFloat marshal = new MarshalFloat();
		marshal.register(envelope);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(
				NameSpace.URL);
		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
		// Get boolean into soapArray
		try {
			SoapPrimitive soapResult = (SoapPrimitive) envelope.getResponse();
			if (soapResult.toString().equals("true"))
				return true;
		} catch (SoapFault e) {

			e.printStackTrace();
		}

		return false;
	}

	public void setFragmet() {
		FragmentManager manager = getFragmentManager();
		FragmentTransaction transition = manager.beginTransaction();
		DetailMain frangment = new DetailMain();
		transition.replace(R.id.userTab3, frangment);
		transition.addToBackStack(null);
		transition.commit();
	}

	// load user place
	private String getUserPlace(String UserName) {
		METHOD_NAME = "getUserByName";
		SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
		SoapObject request = new SoapObject(NameSpace.NAMESPACE, METHOD_NAME);
		request.addProperty("name", UserName);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		MarshalFloat marshal = new MarshalFloat();
		marshal.register(envelope);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(
				NameSpace.URL);
		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
		// Get boolean into soapArray
		try {
			SoapObject soapResult = (SoapObject) envelope.getResponse();
			return soapResult.getProperty("UserPlace").toString();
		} catch (SoapFault e) {
			return "loi";
		}
	}
	private class LoadMonanByUserName extends AsyncTask<Void, Void, Void> {
		protected String userName;
		GifView a;
		
		public LoadMonanByUserName(String username) {
			userName = username;
		}

		@Override
		protected Void doInBackground(Void... params) {
			getMonanByUsername(userName);
			return null;
		}

		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
			a= (GifView) findViewById(R.id.imageloading);
			a.loadGIFResource(getApplicationContext(), R.drawable.loader2);
			a.setXY(63, 180);
		}

		@Override
		protected void onPostExecute(Void result) {
			a.setVisibility(View.GONE);
			setMonanForUsername();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}
	
	private void setMonanForUsername() {
		GridView gv = (GridView) findViewById(R.id.gridViewuse);
		gv.setAdapter(adapter);
		gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				inMain=false;
				int index = Integer.parseInt(rank.get(position));
				// set view
				NameSpace.setId(index);
				setFragmet();
			}
		});
		
	}

	// load ghim monan
	private void getMonanByUsername(String userName) {
		METHOD_NAME = "getListMonanByUserName";
		SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;
		rank = new ArrayList<String>();
		// Adapter cho GridView
		itemList = new ArrayList<LowItem>();
		
		// set Datasource cho Adapter
		getAdapter(userName);
		adapter = new LowItemAdapter(getApplicationContext(),
				R.layout.low_item, itemList);
		// gán Adapter vào Gridview
		
	}

	// lay du lieu
	private void getAdapter(String name) {
		try {
			SoapObject request = new SoapObject(NameSpace.NAMESPACE,
					METHOD_NAME);
			request.addProperty("username", name);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);

			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			MarshalFloat marshal = new MarshalFloat();
			marshal.register(envelope);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(
					NameSpace.URL);
			androidHttpTransport.call(SOAP_ACTION, envelope);
			// Get Array Catalog into soapArray
			SoapObject soapArray = (SoapObject) envelope.getResponse();
			// arrCate.clear();
			// soapArray.getPropertyCount() return number of
			// element in soapArray

			for (int i = 0; i < soapArray.getPropertyCount(); i++) {
				// (SoapObject) soapArray.getProperty(i) get item at position i
				SoapObject soapItem = (SoapObject) soapArray.getProperty(i);
				// soapItem.getProperty("CateId") get value of CateId property
				String MonanName = soapItem.getProperty("MonanName").toString();
				String MonanImage = soapItem.getProperty("MonanImage")
						.toString();
				String MonanPlace = soapItem.getProperty("MonanPlace")
						.toString();
				String MonanId = soapItem.getProperty("MonanId").toString();

				LowItem item = new LowItem();
				item.setImage(MonanImage);
				item.setName(MonanName);
				rank.add(MonanId);

				itemList.add(item);
			}
			adapter.notifyDataSetChanged();
		} catch (Exception e) {

		}

	}
}