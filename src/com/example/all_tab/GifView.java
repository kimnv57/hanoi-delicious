package com.example.all_tab;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.util.AttributeSet;
import android.view.View;

public class GifView extends View {

	Movie movie;
	int x=0, y=0;
	long moviestart;
	float scalewidth = 1;
	float scaleheight = 1;
	Context  context;

	public GifView(Context context) throws IOException {
		
		super(context);
	}

	public GifView(Context context, AttributeSet attrs) throws IOException {
		super(context, attrs);
	}

	public GifView(Context context, AttributeSet attrs, int defStyle)
			throws IOException {
		super(context, attrs, defStyle);
	}

	public void loadGIFResource(Context context, int id) {
		// turn off hardware acceleration
		this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		InputStream is = context.getResources().openRawResource(id);
		movie = Movie.decodeStream(is);

	}
	public void setXY(int xx , int yy) {
		x=xx;
		y=yy;
	}
	
	public int getwidth() {
		return movie.width();
	}
	public int getheight() {
		return movie.height();
	}

	public void loadGIFAsset(Context context, String filename) {
		InputStream is;
		try {
			is = context.getResources().getAssets().open(filename);
			movie = Movie.decodeStream(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void scale(float x,float y) {
		scalewidth=x;
		scaleheight=y;
	}

	@Override
	protected void onDraw(Canvas canvas) {
//		int gifwidth = getMeasuredWidth();
//		int gifheight = getMeasuredHeight();
//		int screenwidth = getLayoutParams().width;
//		int screenheight = getLayoutParams().height;
//		float scaleWidth = (float) ((screenwidth / (1f * gifwidth)));
//		float scaleHeight = (float) ((screenheight / (1f * gifheight)));
//		canvas.scale(scaleWidth, scaleHeight);
		
		canvas.scale(scalewidth, scaleheight);
		super.onDraw(canvas);
		
		if (movie == null) {
			return;
		}

		long now = android.os.SystemClock.uptimeMillis();

		if (moviestart == 0)
			moviestart = now;

		int relTime;
		relTime = (int) ((now - moviestart) % movie.duration());

		movie.setTime(relTime);
		movie.draw(canvas, x, y);
		this.invalidate();
	}
}
