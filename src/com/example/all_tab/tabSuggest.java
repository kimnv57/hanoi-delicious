package com.example.all_tab;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import variables.NameSpace;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hanoi_delicious2.AlertDialogManager;
import com.example.hanoi_delicious2.R;
import com.example.hanoi_delicious2.suggestSub;

public class tabSuggest extends Activity {
	ArrayList<Integer> numberId;
	String METHOD_NAME, SOAP_ACTION;
	ProgressDialog mProgressDialog;
	LinearLayout myview ;
	int screenwidth ;
	int screenheight;

	ImageView pictureSuggest;
	TextView nameSuggest;
	TextView placeSuggest;
	TextView contenSuggest;
	RatingBar rateSuggest;
	Button nextSuggest;
	Button startSuggest;
	
	
	String monanName ;
	String monanImage ;
	String monanPlace;
	String monanPoint;
	String monanSuggest;
	
	AlertDialogManager alert = new AlertDialogManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment2);
		
		// cai dat de vao mang
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		setUI1();

	}

	private void setUI1() {
		startSuggest = (Button) findViewById(R.id.startsuggest);
		startSuggest.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setContentView(R.layout.suggest_tab);
				loadUI();
				new LoadSuggest().execute();
			}
		});
		contenSuggest = (TextView) findViewById(R.id.suggestContent);
		contenSuggest.setText("Hãy để chúng tôi gợi ý món ăn cho bạn nhé");
		final GifView a= (GifView) findViewById(R.id.waiterImage);
		a.loadGIFResource(getApplicationContext(), R.drawable.hd1);
		final int gifwidth = a.getwidth();
		final int gifheight = a.getheight();
		myview = (LinearLayout) findViewById(R.id.LinearWaiter);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		final int height = displaymetrics.heightPixels;
		final int width = displaymetrics.widthPixels;
		
		myview.post(new Runnable() 
	    {

	        @Override
	        public void run()
	        {
	        	screenwidth=width;
	        	screenheight=height-40;
	        	float scaleWidth = (float) (((screenwidth*(0.4)) / (1f * gifwidth)));
	    		float scaleHeight = (float) (((screenheight*0.5) / (1f * gifheight)));
	    		a.scale(scaleWidth,scaleHeight);
	    		//Toast.makeText(getApplicationContext(), "hi"+scaleWidth+"    "+ scaleHeight, Toast.LENGTH_LONG).show();
	        }
	    });
		
	}
	
	private class LoadSuggest extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			loadNumber();
			
			Random myrandom = new Random();
			int num = myrandom.nextInt(numberId.size());
			int id = numberId.get(num);
			numberId.remove(num);
			getUiMonan(id);
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(tabSuggest.this);
			// Set progressdialog title
			mProgressDialog.setTitle("Hà Nội Delicious");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			setUIMonan();
			setlisten();
			mProgressDialog.dismiss();
			new DownloadImageTask(pictureSuggest).execute(monanImage);
		}

		
		
		
	}
	
	private void setUIMonan() {
		rateSuggest.setRating((float) Integer.parseInt(monanPoint) / 10);
		LayerDrawable stars = (LayerDrawable) rateSuggest.getProgressDrawable();
		stars.getDrawable(2).setColorFilter(Color.YELLOW,
				PorterDuff.Mode.SRC_ATOP);
		nameSuggest.setText(monanName);
		placeSuggest.setText("Địa Điểm: " + monanPlace);
		contenSuggest.setText(monanSuggest);
		
	}
	public void setFragmet() {
		FragmentManager manager = getFragmentManager();
		FragmentTransaction transition = manager.beginTransaction();
		suggestSub frangment = new suggestSub();
		transition.replace(R.id.about, frangment);
		transition.addToBackStack(null);
		transition.commit();
	}

	private void getUiMonan(int num) {
		METHOD_NAME = "getMonan";
		SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;

		try {
			SoapObject request = new SoapObject(NameSpace.NAMESPACE,
					METHOD_NAME);
			request.addProperty("id", num);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			MarshalFloat marshal = new MarshalFloat();
			marshal.register(envelope);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(
					NameSpace.URL);
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapObject soapItem = (SoapObject) envelope.getResponse();

			monanName = soapItem.getProperty("MonanName").toString();
			 monanImage = soapItem.getProperty("MonanImage").toString();
			 monanPlace = soapItem.getProperty("MonanPlace").toString();
			 monanPoint = soapItem.getProperty("Point").toString();
			 monanSuggest = soapItem.getProperty("MonanSuggest")
					.toString();
			

		} catch (Exception e) {
			// Toast.makeText(getActivity().getApplicationContext(),
			// e.toString(),
			// Toast.LENGTH_LONG).show();
		}

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			
		}

	}

	private int nextRandom() {
		Random a = new Random();
		int mynum = a.nextInt(numberId.size());
		return mynum;
	}

	private void loadUI() {
		pictureSuggest = (ImageView) findViewById(R.id.ImageSuggest);
		nameSuggest = (TextView) findViewById(R.id.nameSugest);
		placeSuggest = (TextView) findViewById(R.id.placeSuggest);
		rateSuggest = (RatingBar) findViewById(R.id.ratingSuggest);
		nextSuggest = (Button) findViewById(R.id.nextSuggestBT);
		contenSuggest = (TextView) findViewById(R.id.suggestContent);
		final GifView a= (GifView) findViewById(R.id.waiterImage1);
		a.loadGIFResource(getApplicationContext(), R.drawable.hd1);
		final int gifwidth = a.getwidth();
		final int gifheight = a.getheight();
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		final int height = displaymetrics.heightPixels;
		final int width = displaymetrics.widthPixels;
		
		myview.post(new Runnable() 
	    {

	        @Override
	        public void run()
	        {
	        	screenwidth=width;
	        	screenheight=height-40;
	        	float scaleWidth = (float) (((screenwidth*(0.4)) / (1f * gifwidth)));
	    		float scaleHeight = (float) (((screenheight*0.35) / (1f * gifheight)));
	    		a.scale(scaleWidth,scaleHeight);
	    		//Toast.makeText(getApplicationContext(), "hi"+scaleWidth+"    "+ scaleHeight, Toast.LENGTH_LONG).show();
	        }
	    });

	}

	private void loadNumber() {
		METHOD_NAME = "getListIdMonan";
		SOAP_ACTION = NameSpace.NAMESPACE + METHOD_NAME;

		try {
			SoapObject request = new SoapObject(NameSpace.NAMESPACE,
					METHOD_NAME);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			MarshalFloat marshal = new MarshalFloat();
			marshal.register(envelope);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(
					NameSpace.URL);
			androidHttpTransport.call(SOAP_ACTION, envelope);
			// Get Array Catalog into soapArray
			SoapObject soapArray = (SoapObject) envelope.getResponse();
			numberId = new ArrayList<Integer>();
			for (int i = 0; i < soapArray.getPropertyCount(); i++) {
				numberId.add(Integer.valueOf(soapArray.getPropertyAsString(i)));
			}
		} catch (Exception e) {
			Log.d("suggest", "loi");
		}
	}

	private void setlisten() {
		nextSuggest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (numberId.size() != 0) {
					new LoadSuggestSub().execute();
				} else {
					alert.showAlertDialog(tabSuggest.this,"Xin chào", "Bạn đã xem hết rồi :)", true);
				}
			}
		});

	}
	private class LoadSuggestSub extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			
			Random myrandom = new Random();
			int num = myrandom.nextInt(numberId.size());
			int id = numberId.get(num);
			numberId.remove(num);
			getUiMonan(id);
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(tabSuggest.this);
			// Set progressdialog title
			mProgressDialog.setTitle("Hà Nội Delicious");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			setUIMonan();
			setlisten();
			mProgressDialog.dismiss();
			new DownloadImageTask(pictureSuggest).execute(monanImage);
		}

		
		
		
	}
}