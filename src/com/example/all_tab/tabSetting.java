﻿package com.example.all_tab;

import com.example.hanoi_delicious2.AlertDialogManager;
import com.example.hanoi_delicious2.R;
import com.example.hanoi_delicious2.R.id;
import com.example.hanoi_delicious2.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
 
public class tabSetting extends Activity{
	AlertDialogManager alert = new AlertDialogManager();
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_tab);
		TextView about= (TextView) findViewById(R.id.about);
		about.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				alert.showAlertDialog(tabSetting.this, "about us", "Nhóm 17 \n"
						+ "      Nguyễn Thị Thảo \n      Nguyễn Văn Kim \n "
						+ "     Nguyễn Bá Lâm \n      Vũ Như Hùng \n    "
						+ "  Trần Lê Vương \n      Chu Quỳnh Trang", true);
				
			}
		});
	}
}