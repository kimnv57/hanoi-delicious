package variables;

import android.graphics.Color;

public class MyColor {
	public static int MYGREY = Color.rgb(230, 230, 230);
	public static int MYQUITEGREY = Color.rgb(242, 242, 242);
	public static int STRONGORAGE = Color.rgb(253, 151, 6);
	public static int BLACK = Color.rgb(46, 39, 33);

}
